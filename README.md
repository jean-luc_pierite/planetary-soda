# Planetary Soda

## Initial Idea/Concept of the Project

### Before we created the Planetary Soda idea/concept, we began by learning and researching more about overlooked global problems & challenges on the topic of food and nutrition. We started going through content on World Health Organization, Food and Agriculture Organization and, eventually, the Global Nutrition Report. There, we found something really interesting and bizarre: of all the school-aged children in the world, _about a third (30.3%) of them do not eat any fruit daily, yet 43.7% consume soda every day_.tc. Therefore, Planetary Soda consists of a critique of the current state of malnutrition and illness in the world. Our intervention aims to further highlight the ridiculousness and unacceptability of this crisis. 

### References/Bibliography:
[Malnutrition: It's More Than Hunger](https://www.who.int/news-room/commentaries/detail/malnutrition-it-s-about-more-than-hunger)

[Malnutrition is a World Health Crisis](https://www.who.int/news/item/13-07-2020-as-more-go-hungry-and-malnutrition-persists-achieving-zero-hunger-by-2030-in-doubt-un-report-warns)

[Zero Hunger 2030 in Doubt](https://www.who.int/news/item/13-07-2020-as-more-go-hungry-and-malnutrition-persists-achieving-zero-hunger-by-2030-in-doubt-un-report-warns)

[Food Security and Nutrition in the World 2020](https://www.who.int/news-room/commentaries/detail/malnutrition-it-s-about-more-than-hunger)

[WHO Urges Government to Promote Healthy Food in Public Facilities](https://www.who.int/news/item/12-01-2021-who-urges-governments-to-promote-healthy-food-in-public-facilities)

[Strategy and Vision For FAOs Work in Nutrition](http://www.fao.org/3/i4185e/i4185e.pdf)

[Maintaining A Healthy Diet During COVID](http://www.fao.org/3/ca8380en/CA8380EN.pdf)

[Global Nutrition Report 2020](https://globalnutritionreport.org/reports/2020-global-nutrition-report/)

[Global Nutrition Report 2018](https://globalnutritionreport.org/reports/global-nutrition-report-2018/executive-summary/)

## Logo

![Planetary Soda Logo](Pictures/LOGO_COMPLETED.png)

## DIY Dehydrator
Parts List:
1. Lamp
- EcoClassic Philips:
    - 53 W
    - 220-240 V 
    - Dimmable: Yes
    - Technology: Halogen
    - Colour Temperature (Kelvin): 2800K - Extra Warm White
2. Fan
- Nidec BETA V
- 12 V
- 0.28 A
3. Aluminium Foil
4. Box

## Carbonated Drink Recipe
1. Add Baking Soda 
2. Add Dehydrated Fruit Powder(s) (Instead of Sugar)
3. Add Lemon Juice
4. Add Microgreens 

## End Artifacts

![End Artifact 1](Pictures/End_Artifact_1.jpg)

![End Artifact 2](Pictures/End_Artifact_2.jpg)
