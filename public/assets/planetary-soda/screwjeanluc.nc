(screwjeanluc.nc)
(----------------------------------------------------------------)
( Generated for RhinoCam to export for CNC-STEP models Raptor X-SL3000s20 / T-Rex1215 )
( Postprocessor written at FabLab BCN/IaaC by eduardo.chamorro@iaac.net )
( Last edit : 22 January - 2019 )
(----------------------------------------------------------------)
(Stock Size X = 2500.0000, Y =  1200.0000, Z = 15.1000)
(Home Origin X =  0, Y = 0, Z = 0)
(Units = MM, Spindle Speed = 18000)
(Max cut depth = Z-15.1000)
(Tool dia= 6.0 mm, Tool length= 90.0 mm)(WARNING!-CHECK YOUR TOOL)
(-------------------------------------------------------------)
%
G90
G64
M7
M8
T1
S18000
M3
G1 X1349.0000 Y592.0000 Z20.0000 F12000.
G1 X1349.0000 Y592.0000 Z-2.0000 F4000.
G1 X1349.0000 Y592.0000 Z20.0000 F12000.
G1 X728.0000 Y608.0000 Z20.0000 F12000.
G1 X728.0000 Y608.0000 Z-2.0000 F4000.
G1 X728.0000 Y608.0000 Z20.0000 F12000.
M9
M5
M30
(--THE END - THANK YOU FOR USING THIS AWESOME POSTPROCESSOR---)
